import React, {useState} from 'react'
import gql from 'graphql-tag'
import InvoiceBox from '../components/InvoiceBox'
import InvoiceAccountBox from '../components/InvoiceAccountBox'
import NewPet from '../components/NewPet'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loader from '../components/Loader'

const PERSON_DETAILS = gql`
  fragment PersonneDetails on CONSULTERPERSONNE__personne {
    id
    nom
    prenom
  }
`

const INVOICE_ACCOUNTS = gql`
  fragment InvoiceAccounts on CONSULTERCOMPTESFACTURATIONPERSONNE__listecomptefacturation {
    items {
      id
    }
  }
`

const GET_INVOICE_ACCOUNTS_BY_PERSON_ID = gql`
  query getInvoiceAccountsByPerson($id: String!) {
    person: consulterPersonne(id: $id) {
      ...PersonneDetails
      accounts: comptesFacturation {
        ...InvoiceAccounts
      }
    }
  }
  ${PERSON_DETAILS}
  ${INVOICE_ACCOUNTS}
`

const GET_INVOICES = gql`
  query getInvoices($idCP: [String]) {
    factures(idCP: $idCP) {
      comptesFacturation {
        invoices: factures {
          idFacture
        }
      }
    }
  }
`

// const CREATE_PET = gql`
//   mutation CreatePet($input: NewPetInput!) {
//     addPet(input: $input) {
//       ...PetDetails
//     }
//   }
//   ${PET_DETAILS}
// `;

export default function Invoices () {
  const [modal, setModal] = useState(false)
  const listAccounts = useQuery(GET_INVOICE_ACCOUNTS_BY_PERSON_ID, {
    variables: { "id": "800002028960"},
  }) 
  // THIS DOESN'T WORK WHY? 
  const listInvoices = useQuery(GET_INVOICES, {
    variables: { "idCP": ["40008404", "60000267536"]}, skip: listAccounts === undefined
  })

  // const [createPet, newPet] = useMutation(CREATE_PET, {
  //   update(cache, { data: { addPet } }) {
  //     const { pets } = cache.readQuery({ query: GET_PETS })

  //     cache.writeQuery({
  //       query: GET_PETS,
  //       data: { pets: [addPet, ...pets] }
  //     })
  //   }
  // })

  if (listAccounts.loading) return (
    <section className="section is-grey-lighter is-tri">
      <div className="container is-fluid is-medium is-tri has-text-centered">
        <Loader />
      </div>
    </section>
  )
  if (listAccounts.error) return (
    <section className="section is-grey-lighter is-tri">
      <div className="container is-fluid is-medium is-tri has-text-centered">
        <h3 className="title is-size-3 is-tri has-text-primary">
          {`\u26A0 OOPS!! GRAPHQL ERROR`}
        </h3>
      </div>
    </section>
  )
  // if (invoices.error || newPet.error) return <p>ERROR</p>

  const onSubmit = input => {
    setModal(false)
    createPet({
      variables: {input},
    
      optimisticResponse: {
        __typename: 'Mutation',
        addPet: {
          __typename: 'Pet',
          id: Math.round(Math.random() * -1000000) + '',
          type: input.type,
          name: input.name,
          img: 'https://via.placeholder.com/300',
          vacinated: true
        }
      }
    })
  }

  const invoiceAccounts = listAccounts.data.person.accounts.items.map(account => (
    <div className="box is-shadowless is-inline" key={account.id}>
      <InvoiceAccountBox account={account} />
    </div>
  ))

  // const invoiceList = invoices.data.person.accounts.items.map(invoice => (
  //   <div className="col-xs-12 col-md-4 col" key={invoice.id}>
  //     <div className="box">
  //       <InvoiceBox invoice={invoice} />
  //     </div>
  //   </div>
  // ))
  
  if (modal) {
    return (
      <div className="row center-xs">
        <div className="col-xs-8">
          <NewPet onSubmit={onSubmit} onCancel={() => setModal(false)}/>
        </div>
      </div>
    )
  }

  return (
    <div className="invoices-page">
      <section className="hero is-small has-pattern-dark is-overlapped is-tri">
        <div className="hero-body">
          <div className="container is-fluid is-medium is-tri">
            <div className="columns is-centered">
              <div className="column is-12">
                <h3 className="title is-size-3 is-tri">
                  {`Personne ID : ${listAccounts.data.person.id} | Name : ${listAccounts.data.person.prenom} ${listAccounts.data.person.nom}`}
                </h3>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section is-grey-lighter is-tri">
        <div className="container is-fluid is-medium is-tri">
          <div className="box is-flat is-flat-white is-tri">
            <h3 className="title is-size-3 is-tri">
              {`Comptes de Facturation`}
            </h3>
            <div className="columns">
              <div className="column is-vcentered is-8">
                  {invoiceAccounts}
              </div>
              <div className="column is-4">
                <div className="text button is-primary is-tri" onClick={() => setModal(true)}>
                  <span>Filtrer par compte de facturation</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section is-grey-lighter is-tri">
        <div className="container is-fluid is-medium is-tri">
            {/* {invoiceList} */}
        </div>
      </section>
    </div>
  )
}
