import React, {useState} from 'react'
import gql from 'graphql-tag'
import CharacterBox from '../components/CharacterBox'
import NewPet from '../components/NewPet'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loader from '../components/Loader'

const CHARACTER_DETAILS = gql`
  fragment CharacterDetails on Character {
    id
    name
    image
  }
`

const GET_ALL_CHARACTERS = gql`
  query getAllCharactuers($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      results {
        ...CharacterDetails
      }
    }
  }
  ${CHARACTER_DETAILS}
`

// const CREATE_PET = gql`
//   mutation CreatePet($input: NewPetInput!) {
//     addPet(input: $input) {
//       ...PetDetails
//     }
//   }
//   ${PET_DETAILS}
// `;

export default function Pets () {
  const [modal, setModal] = useState(false)
  const characters = useQuery(GET_ALL_CHARACTERS, {
    variables: { "page": 2, "filter": { "name": "Rick"}},
  })

  // const [createPet, newPet] = useMutation(CREATE_PET, {
  //   update(cache, { data: { addPet } }) {
  //     const { pets } = cache.readQuery({ query: GET_PETS })

  //     cache.writeQuery({
  //       query: GET_PETS,
  //       data: { pets: [addPet, ...pets] }
  //     })
  //   }
  // })

  if (characters.loading) return <Loader />
  if (characters.error) return <p>ERROR</p>
  // if (invoices.error || newPet.error) return <p>ERROR</p>

  const onSubmit = input => {
    setModal(false)
    createPet({
      variables: {input},
    
      optimisticResponse: {
        __typename: 'Mutation',
        addPet: {
          __typename: 'Pet',
          id: Math.round(Math.random() * -1000000) + '',
          type: input.type,
          name: input.name,
          img: 'https://via.placeholder.com/300',
          vacinated: true
        }
      }
    })
  }

  const characterList = characters.data.characters.results.map(character => (
    <div className="col-xs-12 col-md-4 col" key={character.id}>
      <div className="box-pet">
        <CharacterBox character={character} />
      </div>
    </div>
  ))
  
  if (modal) {
    return (
      <div className="row center-xs">
        <div className="col-xs-8">
          <NewPet onSubmit={onSubmit} onCancel={() => setModal(false)}/>
        </div>
      </div>
    )
  }

  return (
    <div className="page pets-page">
      <section>
        <div className="row betwee-xs middle-xs">
          <div className="col-xs-10">
            <h1>Characters</h1>
          </div>

          <div className="col-xs-2">
            <button onClick={() => setModal(true)}>new character</button>
          </div>
        </div>
      </section>
      <section>
        <div className="row">
          {characterList}
        </div>
      </section>
    </div>
  )
}
