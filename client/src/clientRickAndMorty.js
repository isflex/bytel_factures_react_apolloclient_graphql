import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'

const endpoint = 'https://rickandmortyapi.com/graphql'

const delay = setContext(
  request =>
    new Promise((success, fail) => {
      setTimeout(() => {
        success()
      }, 800)
    })
)

const cache = new InMemoryCache()
const http = new HttpLink({
  uri: endpoint
})

const link = ApolloLink.from([
  delay,
  http
])

const clientRickAndMorty = new ApolloClient({
  cache,
  link
})

export default clientRickAndMorty
