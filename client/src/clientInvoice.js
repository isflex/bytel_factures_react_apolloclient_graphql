import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'

const endpoint = 'https://ap2.gateway.graphql.tbd.euw3r53.nbyt.fr:8443/'

const delay = setContext(
  request =>
    new Promise((success, fail) => {
      setTimeout(() => {
        success()
      }, 800)
    })
)

const cache = new InMemoryCache()
const http = new HttpLink({
  uri: endpoint
})

const link = ApolloLink.from([
  delay,
  http
])

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  // const token = localStorage.getItem('token');
  const token = "at-5cb85b31-eb01-4c85-b651-c5482d8ef69b"
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  }
});

const clientInvoice = new ApolloClient({
  cache,
  link: authLink.concat(link)
})

export default clientInvoice
