import { Switch, Route } from 'react-router-dom'
import React, {Fragment} from 'react'
import Header from './Header'
import Pets from '../pages/Pets'
import Invoices from '../pages/Invoices'
import RichAndMorty from '../pages/RichAndMorty'

const App = () => (
  <Fragment>
    <Header />
    <div>
      <Switch>
        {/* <Route exact path="/" component={Pets} /> */}
        <Route exact path="/" component={Invoices} />
        {/* <Route exact path="/" component={RichAndMorty} /> */}
      </Switch>
    </div>
  </Fragment>
)

export default App
