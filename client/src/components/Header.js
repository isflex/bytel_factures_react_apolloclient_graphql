import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'

const Header = () =>
  <header className="is-fullwidth">
    <nav className="navbar is-new is-tri" aria-label="main navigation" role="navigation">
      <Link to="/" >
        <div className="navbar-brand is-tri">
          <a className="navbar-item is-tri">
            <img src="https://b2b/a7/header@0.0.1/dist/logo.fd982cd2.svg" alt="Bouygues Telecom" />
          </a>
          <div className="navbar-title is-tri">
            <h1 className="title is-size-1 is-tri">Factures | React | Apollo Client | GraphQL</h1>
          </div>
        </div>
      </Link>
    </nav>
  </header>

// const Header = () =>
//   <header class='pets'>
//     <div className="row">
//       <div className="col-xs">
//         <Link to="/" >
//           Home
//         </Link>
//       </div>
//     </div>
//   </header>

export default withRouter(Header)
