import React from 'react'

const InvoiceBox = ({invoice}) => (
  <div className="pet">
    <figure>
      <img src={invoice.image} alt=""/>
    </figure>
    <div className="pet-type">{invoice.id}</div>
    <div className="pet-name">{invoice.name}</div>
  </div>
)

export default InvoiceBox
