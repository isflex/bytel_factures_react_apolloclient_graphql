import React from 'react'

const InvoiceAccountBox = ({account}) => (
  <div className="tag invoice-account">
    <div className="account-id">{account.id}</div>
  </div>
)

export default InvoiceAccountBox
