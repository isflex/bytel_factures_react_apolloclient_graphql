import React from 'react'

const CharacterBox = ({character}) => (
  <div className="pet">
    <figure>
      <img src={character.image} alt=""/>
    </figure>
    <div className="pet-type">{character.id}</div>
    <div className="pet-name">{character.name}</div>
  </div>
)

export default CharacterBox
